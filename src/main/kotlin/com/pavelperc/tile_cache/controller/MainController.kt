package com.pavelperc.tile_cache.controller

import com.pavelperc.tile_cache.service.CacheService
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono
import reactor.core.scheduler.Schedulers


@RestController
class MainController(
        val cacheService: CacheService
) {
    
    @GetMapping
    fun hello(): String {
        return "Hello. Usage format is /x/y/z.png"
    }
    
    
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    class ImageNotFoundException(x: Int, y: Int, z: Int) : RuntimeException("No image with coords $x/$y/$z.")
    
    data class Pos(val x: Int, val y: Int, val z: Int)
    
    val monoCache = mutableMapOf<Pos, Mono<ByteArray?>>()
    
    @GetMapping("/{x}/{y}/{z}.png", produces = [MediaType.IMAGE_PNG_VALUE])
    fun getTile(
            @PathVariable x: Int,
            @PathVariable y: Int,
            @PathVariable z: Int
    ): Mono<ByteArray?> {
        return monoCache[Pos(x, y, z)] ?: Mono
                .fromSupplier { cacheService.getCachedImage(x, y, z) }
                .switchIfEmpty(Mono.fromSupplier {
                    cacheService.getUrlImage(x, y, z).also { cacheService.cacheImage(x, y, z, it) }
                })
                .publishOn(Schedulers.parallel())
                .also { mono -> monoCache[Pos(x, y, z)] = mono }


//        return URL("https://a.tile.openstreetmap.org/$x/$y/$z.png").readBytes()
    }
}