package com.pavelperc.tile_cache.service

import com.pavelperc.tile_cache.controller.MainController
import org.springframework.stereotype.Service
import java.io.File
import java.io.FileNotFoundException
import java.net.URL

@Service
class CacheService {
    
    fun getCachedFile(x: Int, y: Int, z: Int) =
            File("tiles/${x}_${y}_$z.png")
    
    fun getCachedImage(x: Int, y: Int, z: Int): ByteArray? {
        val bytes = getCachedFile(x, y, z).run { if (canRead()) readBytes() else null }
        if (bytes == null) {
            println("no cache for $x/$y/$z.")
        } else {
            println("load cached for $x/$y/$z.")
        }
        return bytes
    }
    
    fun cacheImage(x: Int, y: Int, z: Int, bytes: ByteArray) {
        println("saved cache for $x/$y/$z.")
        getCachedFile(x, y, z).apply {
            parentFile.mkdirs()
            writeBytes(bytes)
        }
    }
    
    fun getUrlImage(x: Int, y: Int, z: Int): ByteArray {
        println("Start loading url for $x/$y/$z.")
        Thread.sleep(2000)
        
        val bytes = try {
            URL("https://a.tile.openstreetmap.org/$x/$y/$z.png").readBytes()
        } catch (e: FileNotFoundException) {
            throw MainController.ImageNotFoundException(x, y, z)
        }
        
        println("load url for $x/$y/$z.")
        return bytes
    }
    
    fun deleteCache() {
        File("tiles").deleteRecursively()
    }
}