package com.pavelperc.tile_cache

import com.pavelperc.tile_cache.service.CacheService
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import java.io.File

@SpringBootApplication
class TileCacheApplication

fun main(args: Array<String>) {
    CacheService().deleteCache()
    
    runApplication<TileCacheApplication>(*args)
}
