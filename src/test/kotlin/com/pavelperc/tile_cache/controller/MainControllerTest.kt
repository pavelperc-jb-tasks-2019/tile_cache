package com.pavelperc.tile_cache.controller

import com.pavelperc.tile_cache.service.CacheService
import org.amshove.kluent.*
import org.junit.Test
import reactor.core.scheduler.Schedulers


class FakeCacheService(
        val logger: MutableList<String> = mutableListOf()
) : CacheService() {
    private val caches = mutableSetOf<Int>()
    
    override fun getCachedImage(x: Int, y: Int, z: Int): ByteArray? {
        return if (x !in caches) {
            logger += "no cache"
            null
        } else {
            logger += "cached"
            byteArrayOf(x.toByte())
        }
    }
    
    override fun cacheImage(x: Int, y: Int, z: Int, bytes: ByteArray) {
        logger += "save"
        caches += x
    }
    
    override fun getUrlImage(x: Int, y: Int, z: Int): ByteArray {
        Thread.sleep(2000)
        logger += "url"
        return byteArrayOf(x.toByte())
    }
}

class MainControllerTest {
    
    @Test
    fun testGetTile() {
        
        val cacher = FakeCacheService()
        val controller = MainController(cacher)
        controller.cacheService shouldBeInstanceOf FakeCacheService::class
        
        val mono1 = controller.getTile(1, 2, 3)
        val mono2 = controller.getTile(1, 2, 3)
        
//        mono1 shouldBe mono2
        
        mono1
//                .subscribeOn(Schedulers.immediate())
                .subscribe { it.shouldNotBeNull().shouldContain(1); println("s1") }
        mono2
                .subscribeOn(Schedulers.immediate())
                .subscribe { it.shouldNotBeNull().shouldContain(1); println("s2") }
        
        
        cacher.logger shouldEqual listOf(
                "no cache", // first
                "url",
                "save",
                "cached" // second
        )
        println("cacher")
    }
    
    
}
