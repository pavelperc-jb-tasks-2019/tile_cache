package com.pavelperc.tile_cache

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest
class TileCacheApplicationTests {
    
    @Test
    fun contextLoads() {
    }
    
}
